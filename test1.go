package main

import (
	"fmt"
	"os/exec"
	"io"
	"bufio"
	"os"
)

func main() {
	fmt.Println("Plop ici")

	echoCmd := exec.Command("/Volumes/STALKER HD 2/projets/symbol-it/go/src/bitbucket.org/symbol-it/stream-cmd-output/streamcmdoutput/strcmd")

	stOut, err := echoCmd.StdoutPipe()

	if err != nil {
		panic(err)
	}

	err = echoCmd.Start()

	if err != nil {
		panic(err)
	}


	go func(reader io.Reader) {
		scanner := bufio.NewScanner(reader)
		for scanner.Scan() {
			fmt.Printf("%s\n", scanner.Text())
		}
		if err := scanner.Err(); err != nil {
			fmt.Fprintln(os.Stderr, "There was an error with the scanner in attached container", err)
		}
	}(stOut)

	err = echoCmd.Wait()

	if err != nil {
		panic(err)
	}

}

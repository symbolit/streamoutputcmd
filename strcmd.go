package main
import (
	"fmt"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Second)

	go func(){
		for t:= range ticker.C {
			fmt.Println("result OK",t)
		}
	}()

	time.Sleep(time.Second * 30)
	ticker.Stop()

}
